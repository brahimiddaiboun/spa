
import React from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import IconButton from '@material-ui/core/IconButton';
import Tooltip, { TooltipProps } from '@material-ui/core/Tooltip';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import * as userActions from 'src/actions/userActions';
import { CustomerList } from 'src/components';
import { Storage } from 'src/utils/localStorage';
import "src/components/Home/Home.scss";

interface IHomeProps {
    userActions:any;
}

const Home = (props: IHomeProps): JSX.Element => {
    const email = window.localStorage.getItem(Storage.EMAIL);

    const BootstrapTooltip = (props: TooltipProps): JSX.Element => {
        return <Tooltip arrow {...props} />;
    }

    const handleLogout = () => {
        props.userActions.logout();
    }
    return (
        <div>
            {email && email.length ? 
                <BootstrapTooltip title="Se deconnecter">
                    <IconButton
                        aria-label="deconnexion"
                        aria-haspopup="true"
                        color="inherit"
                        onClick={handleLogout}
                        href='/'
                    >
                        <ExitToAppIcon />
                    </IconButton>
                </BootstrapTooltip>
            : <div/>}
            <div className="home">
                Liste des clients
                <CustomerList />
            </div>
        </div>
    );
};

const mapStateToProps = (state: any): any => ({
    CustomerReducer: state.customer,
});

const mapDispatchToProps = (dispatch: any): any => ({
    userActions: bindActionCreators(userActions, dispatch)
});

export default withRouter(
    connect<any, any, any>(
        mapStateToProps,
        mapDispatchToProps
    )(Home)
);