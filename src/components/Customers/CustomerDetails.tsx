
import React from 'react';
import "src/components/Customers/CustomerDetails.scss";
import { ICustomerData } from 'src/reducers/interfaces';

interface ICustomerDetailsProps {
    person: ICustomerData;
}

const CustomerDetails = (props: ICustomerDetailsProps): JSX.Element => {
    return (
        <div className="contact-info">
            <section>
                Adresse:
                    <p className="address">
                    {props.person.address}
                </p>
            </section>
        </div>
    );
};


export default CustomerDetails;