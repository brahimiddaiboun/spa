import React, { useState , useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as customerActions from 'src/actions/customerActions';
import { ICustomerReducer } from 'src/reducers/CustomerReducer';
import { ICustomerData } from 'src/reducers/interfaces';
import { CustomerDetails } from 'src/components';
import "src/components/Customers/CustomerList.scss";

interface ICustomerListProps {
    customerActions:any;
    CustomerReducer: ICustomerReducer;
}

const CustomerList = (props: ICustomerListProps): JSX.Element  => {
    useEffect(() => {
        const getClientList = () => {
            props.customerActions.getClientList();
        }
        getClientList();
    }, [props.customerActions]);

    const [person, setContact] = useState(props.CustomerReducer.data[0]);
    const handleClick = (contact: ICustomerData):void => setContact(contact);
    return (
            <div className="app">
                <div className="up">
                    <div className="contacts-container">
                    {props.CustomerReducer.data.map((el:ICustomerData) => {
                            const contactStyles = {
                                backgroundColor: el.id === person.id ? '#7fafdd' : ''
                            }
                            return (
                                <button key={el.id} className="contact" onClick={() => handleClick(el)} style={contactStyles}>
                                    <span className="name">{el.firstname + ' ' + el.lastname}</span>
                                </button>
                            );
                        })}
                    </div>
                </div>
            <div className="down">
                    Detail: 
                    <CustomerDetails person={person} />
                </div>
            </div>
        );
};

const mapStateToProps = (state: any): any => ({
    CustomerReducer: state.customer,
});

const mapDispatchToProps = (dispatch: any): any => ({
    customerActions: bindActionCreators(customerActions, dispatch)
});

export default withRouter(
    connect<any, any, any>(
        mapStateToProps,
        mapDispatchToProps
    )(CustomerList)
);
