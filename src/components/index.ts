
import Home from 'src/components/Home/Home';
import Login from 'src/components/Login/Login'; 
import LoginProtectedRoute from 'src/LoginProtectedRoute/LoginProtectedRoute';
import LoggedLayout from "src/layouts/LoggedLayout";
import CreateUserAccount from "src/components/Login/CreateUserAccount";
import CustomerList from 'src/components/Customers/CustomerList';
import CustomerDetails from  "src/components/Customers/CustomerDetails";
export {
    Home,
    CustomerList,
    CustomerDetails,
    Login,
    CreateUserAccount,
    LoginProtectedRoute,
    LoggedLayout
};