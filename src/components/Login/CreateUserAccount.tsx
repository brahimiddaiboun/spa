import React, { useState} from 'react';
import { withRouter, Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import FormControl from '@material-ui/core/FormControl';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';
import * as userActions from 'src/actions/userActions';
import { ILoginState } from './Login';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: theme.palette.background.default,
        color: theme.palette.primary.dark,
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '90%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    pwdvisibility: {
        display: 'flex',
        alignItems: 'center',
    },
}));

function Alert(props: AlertProps) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

interface ICreateUserAccountProps {
    userActions: any;
    userReducer: any;
}

interface ICreateUserState extends ILoginState {
    showPassword: boolean;
    isCreate: boolean;
}


const CreateUserAccount = (props: ICreateUserAccountProps) => {
    const classes = useStyles();
    const [values, setValues] = useState<ICreateUserState>({
        email: '',
        password: '',
        firstname: '',
        lastname: '',
        showPassword: false,
        isCreate: false,
    });
    
    const handleChange = (prop: keyof ILoginState) => (event: React.ChangeEvent<HTMLInputElement>) => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleCreation = (e) => {
        e.preventDefault();
        props.userActions.createUserAccount({
            email:values.email,
            password:values.password,
            firstname:values.firstname,
            lastname:values.lastname
        });
        setValues({ ...values, isCreate: true });
    };

    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };
    
    const displayCreationMessage = (
        <Snackbar open={props.userReducer.isError} autoHideDuration={6000}>
            <Alert severity={props.userReducer.isError ? "error" : "success"}>
                {props.userReducer.errorMessage}
            </Alert>
        </Snackbar>
    );
    return (
        <Container component="main" maxWidth="md">
            {values.isCreate && <Redirect to='/login' />}
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                  Creation de compte
                </Typography>
                <form className={classes.form} noValidate>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                autoComplete="fname"
                                name="firstName"
                                variant="outlined"
                                required
                                fullWidth
                                id="firstName"
                                label="Prenom"
                                autoFocus
                                onChange={handleChange('firstname')}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="lastName"
                                label="Nom"
                                name="lastName"
                                autoComplete="lname"
                                onChange={handleChange('lastname')}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="email"
                                label="Email"
                                name="email"
                                autoComplete="email"
                                onChange={handleChange('email')}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <FormControl>
                                <div className={classes.pwdvisibility}>
                                    <TextField
                                        variant="outlined"
                                        required
                                        fullWidth
                                        name="password"
                                        label="Mot de passe"
                                        type={values.showPassword ? 'text' : 'password'}
                                        id="password"
                                        value={values.password}
                                        autoComplete="current-password"
                                        onChange={handleChange('password')}
                                    />
                                    {values.showPassword ? <Visibility onClick={handleClickShowPassword} /> : <VisibilityOff onClick={handleClickShowPassword}/>}
                                </div>
                            </FormControl>
                        </Grid>
                    </Grid>
                    <Button
                        onClick={(e) => handleCreation(e)}
                        type="submit"
                        variant="contained"
                        color="primary"
                        href="/login"
                        className={classes.submit}
                    >
                        Création
                    </Button>
                    {displayCreationMessage}
                    <Grid container justify="flex-end">
                        <Grid item>
                            <Link href="/login" variant="body2">
                                je possede deja un compte? connexion
                            </Link>
                        </Grid>
                    </Grid>
                </form>
          </div>
        </Container>
  );
}

const mapStateToProps = (state: any): any => ({
    userReducer: state.user,
});

const mapDispatchToProps = (dispatch: any): any => ({
    userActions: bindActionCreators(userActions, dispatch)
});

export default withRouter(
    connect<any, any, any>(
        mapStateToProps,
        mapDispatchToProps
    )(CreateUserAccount)
);

