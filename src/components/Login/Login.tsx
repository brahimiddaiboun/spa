import React, { useState } from 'react';
import { withRouter, Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';
import * as userActions from 'src/actions/userActions';

function Alert(props: AlertProps) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    paper: {
        margin: theme.spacing(1, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: theme.palette.background.default,
        color: theme.palette.primary.dark,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    pwdvisibility: {
        display: 'flex',
        alignItems: 'center',
    },
}));

interface ILoginProps {
    userActions: any;
    loggedUser: any;
    location: any;
}

export interface ILoginState {
    email: string;
    password: string;
    firstname?: string;
    lastname?: string;
    showPassword: boolean;
}

const handleLogin = (e,props: ILoginProps, state: ILoginState) => {
    e.preventDefault();
    props.userActions.login({
        email: state.email,
        password: state.password
    });
};

const Login = (props: ILoginProps, state: ILoginState): JSX.Element => {
    const classes = useStyles();
    const [values, setValues] = useState<ILoginState>({
        email: '',
        password: '',
        showPassword: false,
    });

    if (props.loggedUser.data.email) {
        const { from } = props.location.state || { from: { pathname: '/' } };
        return <Redirect to={from} />;
    }
    const handleChange = (prop: keyof ILoginState) => (event: React.ChangeEvent<HTMLInputElement>) => {
        setValues({...values, [prop]: event.target.value });
    };

    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };
    const displayLoginTitle = (
        <Typography component="h1" variant="h5">
            Veuillez-vous authentifier : 
        </Typography>
    );

    const displayLoginMessage = (
        <Snackbar open={props.loggedUser.isError} autoHideDuration={6000}>
            <Alert severity={props.loggedUser.isError ? "error" : "success"}>
                {props.loggedUser.errorMessage}
            </Alert>
        </Snackbar>
    );

    const connexion = (
        <Grid container component="main" className={classes.root}>
            <Grid item xs={6} sm={8} md={5} component={Paper} elevation={6} square>
                <div className={classes.paper}>
                    {displayLoginTitle}
                    <form className={classes.form} noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Email"
                            name="email"
                            autoComplete="email"
                            autoFocus
                            onChange={ handleChange('email') }
                        />
                        <FormControl>
                            <div className={classes.pwdvisibility}>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    name="password"
                                    label="Mot de passe"
                                    type={values.showPassword ? 'text' : 'password'}
                                    id="password"
                                    value={values.password}
                                    autoComplete="current-password"
                                    onChange={handleChange('password')}
                                />
                                {values.showPassword ? <Visibility onClick={handleClickShowPassword} /> : <VisibilityOff onClick={handleClickShowPassword} />}
                                </div>
                            </FormControl>
                        <Button
                            onClick={(e)=>handleLogin(e,props, values)}
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                        >
                            Connecte-toi
                        </Button>
                        {displayLoginMessage}
                        <Grid container>
                            <Grid item xs={true}>
                                <Link href="/creationdecompte" variant="body2">
                                    {"Créer un compte ?"}
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Grid>
        </Grid>
    );

    return connexion;
}

const mapStateToProps = (state: any): any => ({
    loggedUser: state.login,
});

const mapDispatchToProps = (dispatch: any): any => ({
    userActions: bindActionCreators(userActions, dispatch)
});

export default withRouter(
    connect<any, any, any>(
        mapStateToProps,
        mapDispatchToProps
    )(Login)
);
