import { push, replace } from 'connected-react-router';

export const asyncRequest = ({ dispatch, getState }): any => (next) => (action) => {
    if (typeof action === 'function') {
        action = action(dispatch, getState);
    }
    const { params, promise, types, ...rest } = action;
    const store = { dispatch, getState };

    if (action.pushToUrl) {
        const state = store.getState();
        const currentQueries = new URLSearchParams(state.router.location.search);
        Object.entries(action.pushToUrl).forEach(([key, values]) => {
            const value: any = values;
            if (value) {
                currentQueries.set(key, value);
            } else {
                currentQueries.delete(key);
            }
        });

        const queriesAsString = currentQueries.toString();
        store.dispatch(
            push({
                pathname: state.router.location.pathname,
                search: `?${queriesAsString}`
            })
        );
    }

    if (action.updateUrl) {
        store.dispatch(push(action.updateUrl));
    }

    if (action.replaceUrl) {
        store.dispatch(replace(action.replaceUrl));
    }
    if (!promise) {
        return next(action);
    }
    const [REQUEST, SUCCESS, FAILURE] = types;

    next({ ...rest, params: params || null, type: REQUEST });
    return promise()
        .then(
            (result) => {
                next({ ...rest, result, type: SUCCESS });
            },
            (error) => {
                next({ ...rest, error, type: FAILURE });
            }
        )
        .catch((error) => {
            next({ ...rest, error, params: params || null, type: FAILURE });
        });
};
