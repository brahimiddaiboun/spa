const constants = {
    USER_LOGOUT: 'USER_LOGOUT',
    USER_LOAD: 'USER_LOAD',
    USER_LOAD_SUCCESS: 'USER_LOAD_SUCCESS',
    USER_LOAD_FAIL: 'USER_LOAD_FAIL',
    LOGIN_LOAD: 'LOGIN_LOAD',
    LOGIN_SUCCESS: 'LOGIN_SUCCESS',
    LOGIN_FAIL: 'LOGIN_FAIL',
    CREATE_USER_LOAD: 'CREATE_USER_LOAD',
    CREATE_USER_SUCCESS: 'CREATE_USER_SUCCESS',
    CREATE_USER_FAIL: 'CREATE_USER_FAIL',
    GET_USER_LOAD: 'GET_USER_LOAD',
    GET_USER_SUCCESS: 'GET_USER_SUCCESS',
    GET_USER_FAIL: 'GET_USER_FAIL',
};
export default constants;
