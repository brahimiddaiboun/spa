export enum Storage {
    USER = 'flaminem-user',
    CLIENT = 'flaminem-client',
    EMAIL = 'flaminem-email',
    ACCOUNT_ID = 'flaminem-account-id',
}
