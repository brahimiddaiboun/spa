/* eslint-disable */
import PropTypes from 'prop-types'; // ES6
import Redbox from 'redbox-react';
import React from 'react';

const CustomErrorReporter = ({ error }) => <Redbox error={error} />;
CustomErrorReporter.propTypes = {
    error: PropTypes.object
};

export default CustomErrorReporter;
