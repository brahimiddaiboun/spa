import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { History } from 'history';
import { ConnectedRouter } from 'connected-react-router';
import { LoginProtectedRoute, LoggedLayout, Login, CreateUserAccount } from 'src/components';
import config from './config';
import * as firebase from 'firebase';
import './App.css';

export const firebaseApp = firebase.initializeApp(config);
export const databaseApp = firebase.database();
interface IAppProps {
  history: History;
}

const App = React.memo((props: IAppProps) => {  
  const histories = props.history;
  return (
    <div className="App">
      <header className="App-header">
        <ConnectedRouter history={histories}>
          <Switch>
            <Route path='/login' component={Login} />
            <Route path='/creationdecompte' component={CreateUserAccount} />
            <LoginProtectedRoute path='/' component={LoggedLayout} />
          </Switch>
        </ConnectedRouter>
      </header>
    </div>
  );
})

export default App;
