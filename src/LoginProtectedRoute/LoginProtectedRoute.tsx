import React, { PureComponent } from 'react';
import { Redirect, Route, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Storage } from 'src/utils/localStorage';

interface IUserProps {
    loginState: any;
    component: any;
}

class LoginProtectedRoute extends PureComponent<IUserProps> {
    render(): JSX.Element {
        const email = window.localStorage.getItem(Storage.EMAIL);
        const { component: Component, loginState, ...rest } = this.props;
        return (
            <Route
                {...rest}
                render={(props) =>
                    loginState.data.email || email ? (
                        <Component {...props} />
                    ) : (
                        <Redirect
                            to={{
                                pathname: '/login',
                                state: { from: props.location }
                            }}
                        />
                    )
                }
            />
        );
    }
}

const mapStateToProps = (state): any => ({
    loginState: state.login
});

export default withRouter(connect<any, any, any>(mapStateToProps)(LoginProtectedRoute));
