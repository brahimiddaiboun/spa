import thunk from 'redux-thunk';
import { applyMiddleware, createStore, compose, Store } from 'redux';
import history from './../utils/history';
import rootReducer from './../reducers';
import { routerMiddleware } from 'connected-react-router';
import { asyncRequest } from './../middlewares/asyncRequest';

let store: Store;

export function configureStore(initialState = {}): Store {
    const middleware = [thunk, asyncRequest, routerMiddleware(history)];
    const middlewareEnhancer = applyMiddleware(...middleware);
    const composeEnhancers = window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] as typeof compose || compose;
    store = createStore(rootReducer(history), initialState, composeEnhancers(middlewareEnhancer));
    return store;
}
