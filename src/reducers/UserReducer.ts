import { createReducer } from 'src/utils';
import rc from 'src/constants/redux-constants';
import { IBaseState, IUserData } from 'src/reducers/interfaces';


export interface IUserReducer extends IBaseState {
    data: IUserData;
}

const initialState: IUserReducer = {
    data: {
        id:'',
        firstname: '',
        lastname: '',
        address: '',
        email: '',
    },
    isError: false,
    isLoading: false,
    errorCode: 'OK',
    errorMessage: ''
};

export default createReducer(initialState, {
    [rc.CREATE_USER_LOAD]: (state, action) => {
        return {
            ...state,
            isError: false,
            isLoading: true
        };
    },
    [rc.CREATE_USER_SUCCESS]: (state, action) => {
        return {
            ...state,
            data: action.result,
            isError: false,
            isLoading: false
        };
    },
    [rc.CREATE_USER_FAIL]: (state, action) => {
        return {
            ...initialState,
            isError: true,
            isLoading: false
        };
    },
});