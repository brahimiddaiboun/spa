import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import user from 'src/reducers/UserReducer';
import login from 'src/reducers/LoginReducer';
import customer from 'src/reducers/CustomerReducer';


const getAppReducer = (history) => {
    return combineReducers({
        router: connectRouter(history),
        user,
        login,
        customer
    });
};

const rootReducer = (history) => {
    const appReducer = getAppReducer(history);
    return (state, action) => appReducer(state, action);
};

export default rootReducer;
