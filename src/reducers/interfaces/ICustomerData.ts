export interface ICustomerData {
    id?: string;
    firstname: string;
    lastname: string;
    address: string;
}
