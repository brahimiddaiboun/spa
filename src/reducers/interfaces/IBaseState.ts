export interface IBaseState {
    isError: boolean;
    isLoading: boolean;
    data: any;
    errorCode?: string;
    errorMessage?: string;
    totalElements?: number;
}
