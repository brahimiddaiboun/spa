export interface IUserData {
    id?: string;
    firstname: string;
    lastname: string;
    address: string;
    email: string;
}
