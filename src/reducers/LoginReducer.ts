import { createReducer } from 'src/utils';
import rc from 'src/constants/redux-constants';
import { IBaseState, IUserData } from 'src/reducers/interfaces';

export interface IUserReducer extends IBaseState {
    data: IUserData;
}

const initialState: IUserReducer = {
    data: {
        firstname: '',
        lastname: '',
        address: '',
        email: '',
    },
    isError: false,
    isLoading: false,
    errorCode: 'OK',
    errorMessage: ''
};

export default createReducer(initialState, {
    [rc.LOGIN_LOAD]: (state, action) => {
        return {
            ...state,
            isError: false,
            isLoading: true
        };
    },
    [rc.LOGIN_SUCCESS]: (state, action) => {
        return {
            ...state,
            data: action.result,
            isError: false,
            isLoading: false
        };
    },
    [rc.LOGIN_FAIL]: (state, action) => {
        return {
            ...state,
            isError: true,
            isLoading: false,
            errorCode: action.error.code,
            errorMessage: action.error.message,
        };
    }
});