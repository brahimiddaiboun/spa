import { createReducer } from 'src/utils';
import rc from 'src/constants/redux-constants';
import { IBaseState, ICustomerData } from 'src/reducers/interfaces';

export interface ICustomerReducer extends IBaseState {
    data: ICustomerData[];
}

const initialState: ICustomerReducer = {
    data: [{
        id: '',
        firstname: '',
        lastname: '',
        address: ''
    }],
    isError: false,
    isLoading: false,
    errorCode: 'OK',
    errorMessage: ''
};

export default createReducer(initialState, {
    [rc.GET_USER_LOAD]: (state, action) => {
        return {
            ...state,
            isError: false,
            isLoading: true
        };
    },
    [rc.GET_USER_SUCCESS]: (state, action) => {
        return {
            ...state,
            data: action.result.data,
            isError: false,
            isLoading: false
        };
    },
    [rc.GET_USER_FAIL]: (state, action) => {
        return {
            ...initialState,
            isError: true,
            isLoading: false
        };
    },
});