export interface IAction {
    types?: string[];
    promise?: any;
    type?: string;
    result?: any;
    pushToUrl?: any;
    isError?: boolean;
    code?: number;
}
