import { IAction } from 'src/actions/interfaces/IAction';

export interface IActionsNotifications {
    (dispatch: any, getState: any): IAction;
}
