import rc from 'src/constants/redux-constants';
import { IAction } from 'src/actions/interfaces';
import Services from 'src/services';
import { Storage } from 'src/utils/localStorage';
import { ILoginState } from 'src/components/Login/Login';

export const createUserAccount = (data: ILoginState): any => {
    return (dispatch: any, getState: any): any => {
        dispatch({
            types: [rc.CREATE_USER_LOAD, rc.CREATE_USER_SUCCESS, rc.CREATE_USER_FAIL],
            promise: () => {
                return Services.UserService.createUserAccount(data).then((res) => {
                    return res;
                });
            }
        }).then(() => {
            const { isError, errorCode, errorMessage } = getState().user;
            return { isError, errorCode, errorMessage };
        });
    }
}

export const login = (data: ILoginState): any => {
    return (dispatch: any, getState: any): any => {
        dispatch({
            types: [rc.LOGIN_LOAD, rc.LOGIN_SUCCESS, rc.LOGIN_FAIL],
            promise: () => {
                return Services.UserService.login(data).then((res) => {
                    window.localStorage.setItem(Storage.EMAIL, JSON.stringify(res.email));
                    window.localStorage.setItem(Storage.ACCOUNT_ID, JSON.stringify(res.id));
                    return res;
                });
            }
        }).then(() => {
            const { isError, errorCode, errorMessage } = getState().user;
            // dispatch(
            //     notificationActions.display({
            //         isError,
            //         errorCode
            //     })
            // );
            return { isError, errorCode, errorMessage };
        });
    }
};

export const logout = (): IAction => {
    window.localStorage.clear();
    return {
        type: rc.USER_LOGOUT
    };
};

