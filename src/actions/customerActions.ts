import rc from 'src/constants/redux-constants';
import { IAction } from 'src/actions/interfaces';
import Services from 'src/services';

export const getClientList = (): any => {
    return (dispatch: any, getState: any): IAction => {
        return dispatch({
            types: [rc.GET_USER_LOAD, rc.GET_USER_SUCCESS, rc.GET_USER_FAIL],
            promise: () => {
                return Services.CustomerServices.getClientList().then((res) => {
                    return res;
                });
            }
        }).then(() => {
            const { isError, errorCode, errorMessage } = getState().user;
            return { isError, errorCode, errorMessage };
        });
    }
}
