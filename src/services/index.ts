import UserService from 'src/services/UserService';
import CustomerServices from 'src/services/CustomerServices';

export default {
    UserService,
    CustomerServices
}
