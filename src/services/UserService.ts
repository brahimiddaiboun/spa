import { ILoginState } from "src/components/Login/Login";
import { firebaseApp } from 'src/App';

export default class UserService {
    static createUserAccount = async (data: ILoginState ): Promise<any> => {
        try {
            await firebaseApp.auth().createUserWithEmailAndPassword(data.email, data.password)
                .then(function (userRecord) {
                    const user = firebaseApp.auth().currentUser;
                    if (!user) return Promise.reject();
                    const displayName = data.firstname + " " + data.lastname;
                    user.updateProfile({
                        displayName
                    }).then(function (resp ) {
                        // Update successful.
                    }).catch((error) => Promise.reject(error));
                })
                .catch((error) => Promise.reject(error));
        } catch (err) {
            return Promise.reject(err);
        }
    };

    static login = async (data: ILoginState): Promise<any> => {
        try {
            const resp = await firebaseApp.auth().signInWithEmailAndPassword(data.email, data.password)
                .then(function (firebaseUser) {
                    return firebaseUser.user;
                })
                .catch(function (error) {
                    return Promise.reject(error);
                });
            if (!resp) return Promise.reject();
            return {
                firstname: resp.displayName || '',
                phone: resp.phoneNumber || '',
                email: resp.email || '',
                id: resp.uid || '',
            }
        } catch (err) {
            return Promise.reject(err);
        }
    };
}
