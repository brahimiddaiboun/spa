
export default class CustomerServices {
    static getClientList = async (): Promise<any> => {
        return Promise.resolve({
            data: [
                {
                    id: '31',
                    firstname: 'Martin',
                    lastname: 'Arthur',
                    address: '14, rue Pergolese, 75116 Paris'
                },
                {
                    id: '32',
                    firstname: 'Dupond',
                    lastname: 'Edouard',
                    address: '27, rue du general de Gaulle, 59300 Valenciennes'
                },
                {
                    id: '33',
                    firstname: 'Lemaitre',
                    lastname: 'Christian',
                    address: '221, Allee des pins, 28550 Dreux'
                },
                {
                    id: '34',
                    firstname: 'Albert',
                    lastname: 'Petis',
                    address: '237, Allee des loups, 28550 Dreux'
                },
                {
                    id: '35',
                    firstname: 'Louis',
                    lastname: 'Legrand',
                    address: '965, rue des pommes, 28550 Dreux'
                },
                {
                    id: '36',
                    firstname: 'Martin',
                    lastname: 'Dilemme',
                    address: '26, place des canards, 28550 Dreux'
                },
                {
                    id: '37',
                    firstname: 'Gerard',
                    lastname: 'Latard',
                    address: '54, boulevard des maurins, 28550 Dreux'
                },
            ]
        });
    };
}
