import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import CustomErrorReporter from 'src/utils/CustomErrorReporter';
import history from 'src/utils/history';
import { configureStore } from 'src/store/configureStore';
import App from 'src/App';
import * as serviceWorker from 'src/serviceWorker';
import 'bootstrap/dist/css/bootstrap.css';
import 'src/index.css';

const initialState = (window as any).__REDUX_STATE__ || {};
const store = configureStore(initialState);

ReactDOM.render(
  <React.StrictMode>
    <AppContainer errorReporter={CustomErrorReporter}>
      <BrowserRouter>
        <Provider store={store}>
          <App history={history} />
        </Provider>
      </BrowserRouter>
    </AppContainer>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
