import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { Home } from 'src/components';

const LoggedLayout = (): JSX.Element => (
    <Switch>
        <Route
            exact
            path='/'
            render={() => {
                return <Redirect to='/home' />;
            }}
        />
        <Route path='/home' component={Home} />
    </Switch>
);

export default LoggedLayout;
